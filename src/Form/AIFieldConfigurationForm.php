<?php

namespace Drupal\ai_field\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Builds the form to OpenAI API configuration.
 */
class AIFieldConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ai_field_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ai_field.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config('ai_field.settings');

    $form['openai_api_key'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#description' => 'Enter the openai API key.',
      '#title' => 'OpenAI API key',
      '#default_value' => $config->get('openai_api_key'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('ai_field.settings')
      ->set('openai_api_key', $values['openai_api_key'])
      ->save();
    parent::submitForm($form, $form_state);
  }

}
