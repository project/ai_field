<?php

namespace Drupal\ai_field\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Orhanerday\OpenAi\OpenAi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a controller for methods used to generate content from AI.
 */
class AIFieldController extends ControllerBase {

  /**
   * Drupal configuration service container.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config) {
    $this->config = $config->get('ai_field.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Function to generate content using AI.
   */
  public function generation($prompt, $message, $model_value, $max_token, $temperature, $presence_penalty, $frequency_penalty, $top_P) {

    $open_ai_key = $this->config->get('openai_api_key');
    $model_key = $model_value;
    $model = $this->listModels()[$model_key];

    $open_ai = new OpenAi($open_ai_key);
    $complete = $open_ai->completion([
      'model' => $model,
      'prompt' => $prompt,
      'temperature' => (float) $temperature,
      'max_tokens' => (int) $max_token,
      'top_p' => (float) $top_P,
      'frequency_penalty' => (float) $frequency_penalty,
      'presence_penalty' => (float) $presence_penalty,
    ]);
    if ($complete) {
      $response = json_decode($complete);
      $data = !empty($response->choices) ? $response->choices[0]->text: '';
      return $data;
    }
  }

  /**
   * Function to list models using AI.
   */
  public function listModels() {
    $model_arr = [];
    $open_ai_key = $this->config->get('openai_api_key');
    $open_ai = new OpenAi($open_ai_key);
    $models = $open_ai->listModels();
    $response = json_decode($models);
    if ($response && property_exists($response, 'data')) {
      foreach ($response->data as $model) {
        array_push($model_arr, $model->id);
      }
    }
    return $model_arr;
  }

}
