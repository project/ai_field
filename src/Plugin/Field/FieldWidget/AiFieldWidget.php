<?php

namespace Drupal\ai_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'field_example_text' widget.
 *
 * @FieldWidget(
 *   id = "ai_field_widget",
 *   module = "ai_field",
 *   label = @Translation("AI Field"),
 *   field_types = {
 *     "ai_field_type"
 *   }
 * )
 */
class AiFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $value1 = $items[$delta]->query ?? '';
    $value2 = $items[$delta]->generated_content ?? '';

    $element += [
      '#type' => 'fieldset',
    ];

    $element['query'] = [
      '#type' => 'textfield',
      '#default_value' => $value1,
      '#size' => 255,
      '#title' => 'Query Prompt',
      '#maxlength' => 255,
    ];

    $element['button'] = [
      '#type' => 'button',
      '#default_value' => 'Generate',
      '#ajax' => [
        'callback' => '\Drupal\ai_field\Plugin\Field\FieldWidget\AiFieldWidget::generate',
        'disable-refocus' => FALSE,
        'wrapper' => 'edit-output',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Please wait...'),
        ],
      ],
      '#button_type' => 'primary',
      '#attribures' => [
        'class' => 'aigenerate',
      ],
    ];

    $element['generated_content'] = [
      '#type' => 'textarea',
      '#default_value' => $value2,
      '#size' => 255,
      '#title' => 'Content',
      '#prefix' => '<div id="edit-output">',
      '#suffix' => '</div>',
    ];

    return $element;
  }

  /**
   * Callback function to generate content.
   */
  public static function generate(array $form, FormStateInterface $form_state) {

    $query = '';
    $field_name = '';
    $values = $form_state->getValues();

    foreach ($form_state->getFormObject()->getEntity()->getFields() as $key => $value) {

      if ($value->getFieldDefinition()->getType() == 'ai_field_type') {

        $settings = $value->getFieldDefinition()->getSettings();
        $max_token = $settings['max_token'];
        $temperature = $settings['temperature'];
        $presence_penalty = $settings['presence_penalty'];
        $frequency_penalty = $settings['frequency_penalty'];
        $top_p = $settings['top_p'];
        $model = $settings['models'];
        $field_name = $key;
        $query = $settings['ai_query'];
        $example_query = $settings['example_query'];
        $example_response = $settings['example_response'];
        $message = [
          'q' => $example_query,
          'r' => $example_response,
        ];
        if (isset($values) && isset($values[$key])) {
          $field_val = $values[$key][0]['query'];
        }

        if ($field_val != '') {
          $ai_query = str_replace('[ai-field:field-value]', $field_val, $query);
          $obj = \Drupal::service('ai_field.ai_field_services');
          $ai_response = $obj->generation($ai_query, $message, $model, $max_token, $temperature, $presence_penalty, $frequency_penalty, $top_p);
          $form[$field_name]['widget'][0]['generated_content']['#value'] = $ai_response;
          return $form[$field_name]['widget'][0]['generated_content'];
        }
      }
    }
  }

}
