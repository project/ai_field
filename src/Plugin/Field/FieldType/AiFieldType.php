<?php

namespace Drupal\ai_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'field_example_rgb' field type.
 *
 * @FieldType(
 *   id = "ai_field_type",
 *   label = @Translation("AI Field"),
 *   module = "ai_field",
 *   description = @Translation("Demonstrates a field composed of an RGB color."),
 *   default_widget = "ai_field_widget",
 *   default_formatter = "ai_field_formatter"
 * )
 */
class AiFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'query' => [
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
        ],
        'generated_content' => [
          'type' => 'varchar',
          'length' => 5000,
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'models' => 'gpt-3.5-turbo-instruct-0914',
      'max_token' => '100',
      'temperature' => '0',
      'presence_penalty' => '0',
      'frequency_penalty' => '0',
      'top_p' => '1',
      'n' => '1',
      'ai_query' => '',
      'example_query' => '',
      'example_response' => '',
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $answer = \Drupal::service('ai_field.ai_field_services');
    $models = $answer->listModels();

    $element['models'] = [
      '#type' => 'select',
      '#title' => 'Select model',
      '#options' => $models,
      '#default_value' => is_numeric($this->getSetting('models')) ? $this->getSetting('models') : array_search($this->getSetting('models'), $models),
      '#required' => TRUE,
      '#validated' => TRUE,
    ];
    $element['max_token'] = [
      '#type' => 'number',
      '#title' => 'Max tokens',
      '#required' => TRUE,
      '#default_value' => $this->getSetting('max_token'),
      '#attributes' => [
        'oninput' => "this.value = this.value.replace(/^0/, '')",
      ],
    ];

    $element['temperature'] = [
      '#type' => 'number',
      '#title' => 'Temperature',
      '#max' => 1,
      '#min' => 0,
      '#step' => 0.1,
      '#default_value' => $this->getSetting('temperature') ?? "0",
    ];

    $element['presence_penalty'] = [
      '#type' => 'number',
      '#title' => 'Presence Penalty',
      '#max' => 2,
      '#min' => -2,
      '#step' => 0.1,
      '#default_value' => $this->getSetting('presence_penalty') ?? "0",
    ];

    $element['frequency_penalty'] = [
      '#type' => 'number',
      '#title' => 'Frequency Penalty',
      '#max' => 2,
      '#min' => -2,
      '#step' => 0.1,
      '#default_value' => $this->getSetting('frequency_penalty') ?? "0",
    ];
    $element['top_p'] = [
      '#type' => 'number',
      '#title' => 'Top_p',
      '#max' => 1,
      '#min' => 0,
      '#step' => 0.1,
      '#default_value' => $this->getSetting('top_p') ?? "1",
    ];

    $element['n'] = [
      '#type' => 'number',
      '#title' => 'Number of result',
      '#max' => 10,
      '#min' => 1,
      '#default_value' => $this->getSetting('n') ?? "1",
    ];
    $element['ai_query'] = [
      '#type' => 'textarea',
      '#size' => 200,
      '#title' => 'AI Query',
      '#required' => TRUE,
      '#description' => 'Enter your AI Query here with available tokens',
      '#default_value' => $this->getSetting('ai_query') ?? "",
    ];

    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $element['tokens'] = [
        '#title' => $this->t('Tokens'),
        '#type' => 'container',
      ];
      $element['tokens']['help'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => [
          'ai-field',
          'current-date',
          'current-page',
          'current-user',
          'language',
          'random',
          'node',
          'site',
        ],
        '#global_types' => FALSE,
        '#dialog' => TRUE,
      ];
    }
    $element['example_query'] = [
      '#type' => 'textarea',
      '#size' => 200,
      '#title' => 'Example Query',
      '#description' => 'Write an example query',
      '#default_value' => $this->getSetting('example_query') ?? "",
    ];

    $element['example_response'] = [
      '#type' => 'textarea',
      '#size' => 255,
      '#title' => 'Example Response',
      '#description' => 'Write an example response how you want output from AI ',
      '#default_value' => $this->getSetting('example_response') ?? "",
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['query'] = DataDefinition::create('string')->setLabel(t('Query Prompt'));

    $properties['generated_content'] = DataDefinition::create('string')->setLabel(t('Generated Content'));

    return $properties;
  }

}
