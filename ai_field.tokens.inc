<?php

/**
 * @file
 * File includes custom code for tokens.
 */

/**
 * Implements hook_token_info().
 */
function ai_field_token_info() {

  $types['ai-field'] = [
    'name' => "AI Field",
    'description' => 'Token related to AI Field',
  ];
  $custom['field-value'] = [
    'name' => "Field Value",
    'description' => 'Field Value to be use in AI Query',
  ];

  return [
    'types' => $types,
    'tokens' => [
      'ai-field' => $custom,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function ai_field_tokens($type, $tokens, array $data, array $options) {
  $replacements = [];
  if ($type == 'ai-field') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'ai_field_value':
          $replacements[$original] = 'abc';
          break;
      }
    }
  }
  return $replacements;
}
