## AI Field
AI Field module allows you to add an AI type field.

## Uses
We can use AI field type as field which can be used to generate content 
using Open AI api.
Step-1: First we need to set Open AI api key in configuration form.
Step-2: Then add field of type AI Field.
Step-3: In field settings form Select model, Max Tokens and AI Query are required fields,
        kindly fill them.
Step-4: In AI Query field in field settings write query prompt starting with "Generate/Create" 
        and use AI Field token from avaialable tokens.
Step-5: Then go to add content and in AI field enter prompt value which will replace token in AI Query.
Step-6: You will get response in Content section of AI Field and then you can save.

## Requirements
 - Drupal 8 or above.
 - Token module (Ensure it is installed and enabled).

## Installation
 - Download the AI Field module.
 - Extract the module to the modules directory of your Drupal installation.
 - Enable the module through the Drupal admin interface or using Drush.
 - Install PHP SDK Package for OpenAI

## Troubleshooting:
 - Verify that the AI Field module is installed and enabled.
 - Check the configuration settings.
